#' Anonimize values
#'
#' @param data The dataframe containing the variables to anonimize.
#' @param varNames The variable names to anonimize.
#'
#' @return The dataframe with the specified columns anonymized.
#' @export
#'
#' @examples anonymizeValues(mtcars[1:10, ], c('cyl', 'vs'));
anonymizeValues <- function(data,
                            varNames,
                            anonymizerOptions = NULL) {
  if (!('data.frame' %in% class(data))) {
    stop("The `data` argument must be a data.frame; instead it ",
         "has class '", class(data), "'.");
  }
  if (!all(varNames %in% names(data))) {
    stop("Not all variables specified in the `varNames` argument ",
         "exist in the dataframe!");
  }
  if (is.null(anonymizerOptions)) {
    anonymizerOptions <-
      list(.algo = "sha256",
           .seed = 0,
           .chars = letters,
           .n_chars = 5L);
  }
  for (currentCol in varNames) {
    data[, currentCol] <-
      do.call(anonymizer_anonymize,
              c(list(.x = data[, currentCol]),
                anonymizerOptions));
  }
  return(data);
}
