# gdpR

R package to support following Open Science directives in a GDPR compliant manner by leveraging the [`Anonymizer`](https://cran.r-project.org/package=anonymizer) and [`Secret`](https://cran.r-project.org/package=anonymizer) packages in project using [LimeSurvey](https://limesurvey.org/) for data collection.

